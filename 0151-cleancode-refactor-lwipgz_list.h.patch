From 7b51e2ad967e1ca11443e82d5c5c9f0bce57990b Mon Sep 17 00:00:00 2001
From: Lemmy Huang <huangliming5@huawei.com>
Date: Thu, 11 Jul 2024 11:52:58 +0800
Subject: [PATCH] cleancode: refactor lwipgz_list.h

  Changed:
    list_is_empty       -> list_head_empty
    list_is_null        -> list_node_null

    init_list_node      -> list_init_head
    init_list_node_null -> list_init_node

    list_add_node       -> list_add_node
    list_del_node_null  -> list_del_node
    list_del_node       -> __list_del_node
    list_for_each_safe  -> list_for_each_node
  Deprecated:
    list_del_node_init
  Added:
    list_get_node_count
    list_entry

Signed-off-by: Lemmy Huang <huangliming5@huawei.com>
---
 src/include/lwipgz_list.h | 94 ++++++++++++++++++++-------------------
 1 file changed, 49 insertions(+), 45 deletions(-)

diff --git a/src/include/lwipgz_list.h b/src/include/lwipgz_list.h
index 3ee6650..9729210 100644
--- a/src/include/lwipgz_list.h
+++ b/src/include/lwipgz_list.h
@@ -33,78 +33,82 @@
 #ifndef __LWIPGZ_LIST_H__
 #define __LWIPGZ_LIST_H__
 
-#ifndef NULL
-#ifdef __cplusplus
-#define NULL 0
-#else
-#define NULL ((void *)0)
-#endif
-#endif
-
+/* double circular linked list */
 struct list_node {
     struct list_node *prev;
     struct list_node *next;
 };
 
-static inline void init_list_node_null(struct list_node *n)
+#ifndef container_of
+#define container_of(ptr, type, member) ({ \
+    typeof( ((type *)0)->member ) *__mptr = (ptr); \
+    (type *)((char *)__mptr - offsetof(type, member));})
+#endif /* container_of */
+
+#define list_entry(ptr, type, member) \
+    container_of(ptr, type, member)
+
+#define list_for_each_node(node, n, head) \
+    for (node = (head)->next, n = (node)->next; \
+         node != (head); \
+         node = n, n = (node)->next)
+
+static inline unsigned list_get_node_count(const struct list_node *h)
 {
-    n->prev = NULL;
-    n->next = NULL;
+    const struct list_node *node, *n;
+    unsigned count = 0;
+    list_for_each_node(node, n, h) {
+        ++count;
+    }
+    return count;
 }
 
-static inline void init_list_node(struct list_node *n)
+static inline int list_node_null(const struct list_node *n)
 {
-    n->prev = n;
-    n->next = n;
+    return (n->prev == NULL) || (n->next == NULL);
 }
 
-static inline void list_add_node(struct list_node *h, struct list_node *n)
+static inline int list_head_empty(const struct list_node *h)
 {
-    n->next = h;
-    n->prev = h->prev;
-    h->prev->next = n;
-    h->prev = n;
+    return h == h->next;
 }
 
-static inline void list_del_node(struct list_node *n)
+static inline void list_init_head(struct list_node *h)
 {
-    struct list_node *prev = n->prev;
-    struct list_node *next = n->next;
-    next->prev = prev;
-    prev->next = next;
+    h->prev = h;
+    h->next = h;
 }
 
-static inline void list_del_node_init(struct list_node *n)
+static inline void list_init_node(struct list_node *n)
 {
-    list_del_node(n);
-    init_list_node(n);
+    n->prev = NULL;
+    n->next = NULL;
 }
 
-static inline void list_del_node_null(struct list_node *n)
+/* add node befor head, means at tail */
+static inline void list_add_node(struct list_node *n, struct list_node *head)
 {
-    if ((n->next) && (n->prev)) {
-        list_del_node(n);
-    }
-    init_list_node_null(n);
+    n->next = head;
+    n->prev = head->prev;
+    head->prev->next = n;
+    head->prev = n;
 }
 
-static inline int list_is_null(const struct list_node *n)
+static inline void __list_del_node(struct list_node *n)
 {
-    return (n->prev == NULL) && (n->next == NULL);
+    struct list_node *prev = n->prev;
+    struct list_node *next = n->next;
+    next->prev = prev;
+    prev->next = next;
 }
 
-static inline int list_is_empty(const struct list_node *h)
+static inline void list_del_node(struct list_node *n)
 {
-    return h == h->next;
+    if (list_node_null(n)) {
+        return;
+    }
+    __list_del_node(n);
+    list_init_node(n);
 }
 
-#define list_for_each_safe(pos, n, head) \
-    for (pos = (head)->next, n = (pos)->next; pos != (head); pos = n, n = (pos)->next)
-
-#ifndef container_of
-#define container_of(ptr, type, member) ({ \
-    typeof( ((type *)0)->member ) *__mptr = (ptr); \
-    (type *)((char *)__mptr - offsetof(type,member));})
-#endif /* container_of */
-
 #endif /* __LWIPGZ_LIST_H__ */
-- 
2.33.0

